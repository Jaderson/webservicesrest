package pt.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pt.controller.TransactionController;
import pt.dto.ResponseDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/java/pt/resource/context-test.xml"})
public class TransactionUT {
	
	private static final String TYPE = "SANDBOX_TAN";

	@Autowired
	TransactionController transactionController;
	
	@Test
	public void listAllTest() {
		ResponseEntity<ResponseDTO> response = transactionController.getTransactions();
		assertTrue(response.getStatusCode() == HttpStatus.OK);
	}
	
	@Test
	public void checkFirstID() {
		ResponseEntity<ResponseDTO> response = transactionController.getTransactions();
		assertEquals(response.getBody().getTransactions().get(0).getId(), "897706c1-dcc6-4e70-9d85-8a537c7cbf3e");
	}
	
	@Test
	public void filterTransactionByType() {
		ResponseEntity<ResponseDTO> response = transactionController.getTransactionsByType(TYPE);
		assertEquals(response.getBody().getTransactions().size(), 2);
	}
	
	@Test
	public void listTransactionForType() {
		ResponseEntity<BigDecimal> response = transactionController.getTotalAmountByType(TYPE);
		assertEquals(response.getBody(), new BigDecimal("10.00"));
	}
	
	@Test
	public void filterTransactionisNull() {
		try {
			transactionController.getTransactionsByType(null);
			fail();
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Not exists of type");
		}
	}
}
