package pt.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OtherAccount {

	private String id;
	private List<Holder> holder;
	private String number;
	private String kind;
	private String IBAN;
	private String swift_bic;
	private List<Bank> bank;
	private List<Metadata> metadata;
}
