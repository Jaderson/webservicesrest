package pt.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName(value = "this_account")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ThisAccount {
	private String id;
	private List<Holders> holders;
	private String number;
	private String kind;
	private String IBAN;
	private String swift_bic;
	private List<Bank> bank;

	}
