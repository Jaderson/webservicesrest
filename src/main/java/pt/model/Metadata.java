package pt.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Metadata {

	private String public_alias;
	private String private_alias;
	private String more_info;
	private String URL;
	private String image_URL;
	private String open_corporates_URL;
	private String corporate_location;
	private String physical_location;
	private String narrative;
	private List<String> comments;
	private List<String> tags;
	private List<String> images;
	private String where;
}
