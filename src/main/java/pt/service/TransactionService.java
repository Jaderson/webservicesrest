package pt.service;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import pt.bo.TransactionBO;
import pt.dto.ResponseDTO;
import pt.exception.TransactionControllerException;
import pt.interfaces.ITransactionService;

@Service
@Component
public class TransactionService implements ITransactionService, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private TransactionBO transactionBO;
	
	public ResponseEntity<ResponseDTO> getTransactions() throws TransactionControllerException {
		return transactionBO.getTransactions();
	}

	public ResponseEntity<ResponseDTO> getTransactionsByType(@PathVariable(name = "transactionType") String transactionType) throws TransactionControllerException {
		return transactionBO.getTransactionsByType(transactionType);
	}

	public ResponseEntity<BigDecimal> getTotalAmountByType(
			@PathVariable(name = "transactionType") String transactionType) throws TransactionControllerException {
		return transactionBO.getTotalAmountByType(transactionType);
	}
}
