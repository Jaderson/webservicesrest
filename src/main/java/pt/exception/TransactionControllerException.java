package pt.exception;

public class TransactionControllerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransactionControllerException() {
	        super();
	    }

	public TransactionControllerException(String message) {
	        super(message);
	    }

}
