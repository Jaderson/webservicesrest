package pt.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pt.model.Transactions;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDTO {
	private List<Transactions> transactions;
}
