package pt.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pt.dto.ResponseDTO;
import pt.exception.TransactionControllerException;
import pt.exception.TransactionControllerHandler.Erro;
import pt.interfaces.ITransactionController;
import pt.service.TransactionService;

@Controller
public class TransactionController implements ITransactionController, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private TransactionService transactionService;

	@RequestMapping(value = "/listAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> getTransactions() throws TransactionControllerException {
		try {
			return transactionService.getTransactions();
		} catch (Exception e) {
			throw new TransactionControllerException(e.getMessage());
		}
	}

	@RequestMapping(value = "/byTransactionsType/{transactionType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> getTransactionsByType(@PathVariable(name = "transactionType") String transactionType)
			throws TransactionControllerException {
		try {
			return transactionService.getTransactionsByType(transactionType);
		} catch (Exception e) {
			throw new TransactionControllerException(e.getMessage());
		}
	}

	@RequestMapping(value = "/totalAmount/{transactionType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BigDecimal> getTotalAmountByType(
			@PathVariable(name = "transactionType") String transactionType) {
		try {
			return transactionService.getTotalAmountByType(transactionType);
		} catch (Exception e) {
			throw new TransactionControllerException(e.getMessage());
		}
	}
	
	@ExceptionHandler({ TransactionControllerException.class })
	public ResponseEntity<Object> TransactionControllerException(TransactionControllerException ex) {
		String messageUser = ex.getMessage();
		String messageDeveloper = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(messageUser, messageDeveloper));
		return ResponseEntity.badRequest().body(erros);
	}
}
