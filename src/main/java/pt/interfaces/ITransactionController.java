package pt.interfaces;

import java.math.BigDecimal;

import org.springframework.http.ResponseEntity;

import pt.dto.ResponseDTO;
import pt.exception.TransactionControllerException;

public interface ITransactionController {

	public ResponseEntity<ResponseDTO> getTransactions() throws TransactionControllerException;

	public ResponseEntity<?> getTransactionsByType(String transactionType) throws TransactionControllerException;

	public ResponseEntity<BigDecimal> getTotalAmountByType(String transactionType)
			throws TransactionControllerException;
}
