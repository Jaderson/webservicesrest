package pt.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import pt.dto.ResponseDTO;
import pt.exception.TransactionControllerException;
import pt.interfaces.ITransactionBO;
import pt.model.Transactions;
import pt.model.Value;

@Component
public class TransactionBO implements ITransactionBO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String ENDPOINT = "https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions";

	public ResponseEntity<ResponseDTO> getTransactions() throws TransactionControllerException {
		ResponseDTO responseDto = null;
		try {
			WebResource webResource = Client.create().resource(ENDPOINT);

			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE).get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new TransactionControllerException("Failed : HTTP error code : " + response.getStatus());
			}

			String output = response.getEntity(String.class);

			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			responseDto = objectMapper.readValue(output, ResponseDTO.class);
		} catch (Exception e) {
			throw new TransactionControllerException("contact the administrator");
		}

		return Optional.of(new ResponseEntity<ResponseDTO>(responseDto, HttpStatus.OK))
				.orElse(new ResponseEntity<ResponseDTO>(new ResponseDTO(), HttpStatus.OK));
	}

	public ResponseEntity<ResponseDTO> getTransactionsByType(@PathVariable(name = "transactionType") String transactionType) throws TransactionControllerException {
		ResponseDTO responseDto = null;
		try {
			WebResource webResource = Client.create().resource(ENDPOINT);

			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE).get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new TransactionControllerException("Failed : HTTP error code : " + response.getStatus());
			}

			if (response.getStatus() == 201) {
				throw new TransactionControllerException("Failed: HTTP no contet " + response.getStatus());
			}

			String output = response.getEntity(String.class);

			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			responseDto = objectMapper.readValue(output, ResponseDTO.class);
		} catch (Exception e) {
			throw new TransactionControllerException("contact the administrator");
		}

		List<Transactions> transactionsFilteredByType = responseDto.getTransactions().stream()
				.filter(t -> t.getDetails() != null && t.getDetails().getType() != null
						&& t.getDetails().getType().equals(transactionType))
				.collect(Collectors.toList());

		if (transactionsFilteredByType.isEmpty()) {
			throw new TransactionControllerException("Not exists of type");
		}

		return Optional
				.of(new ResponseEntity<ResponseDTO>(
						ResponseDTO.builder().transactions(transactionsFilteredByType).build(), HttpStatus.OK))
				.orElseThrow(() -> new TransactionControllerException("Not exists of type"));
	}

	@RequestMapping(value = "/totalAmount/{transactionType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BigDecimal> getTotalAmountByType(
			@PathVariable(name = "transactionType") String transactionType) throws TransactionControllerException {
		ResponseDTO responseDto = null;
		try {
			WebResource webResource = Client.create().resource(ENDPOINT);

			ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_VALUE).get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new TransactionControllerException("Failed : HTTP error code : " + response.getStatus());
			}

			String output = response.getEntity(String.class);

			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			responseDto = objectMapper.readValue(output, ResponseDTO.class);
		} catch (Exception e) {
			throw new TransactionControllerException("contact the administrator");
		}

		List<Transactions> transactionsFilteredByType = responseDto.getTransactions().stream()
				.filter(t -> t.getDetails() != null && t.getDetails().getType() != null
						&& t.getDetails().getType().equals(transactionType))
				.collect(Collectors.toList());
		BigDecimal totalAmount = BigDecimal.ZERO;
		for (Transactions transaction : transactionsFilteredByType) {
			if (transaction.getDetails() != null && transaction.getDetails().getType() != null
					&& transaction.getDetails().getValue() != null) {
				for (Value value : transaction.getDetails().getValue()) {
					totalAmount = totalAmount.add(value.getAmount());
				}
			}
		}
		return Optional.of(new ResponseEntity<BigDecimal>(totalAmount, HttpStatus.OK))
				.orElse(new ResponseEntity<BigDecimal>(BigDecimal.valueOf(0), HttpStatus.OK));
	}
}
