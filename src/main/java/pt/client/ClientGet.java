package pt.client;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import pt.dto.ResponseDTO;


public class ClientGet {
	public static void main(String[] args) {
		try {

			Client client = Client.create();

			WebResource webResource = client.resource("https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions");

			ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			String output = response.getEntity(String.class);
			
			ObjectMapper objectMapper = new ObjectMapper(); 
			objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
			ResponseDTO responseDto = objectMapper.readValue(output, ResponseDTO.class);

			System.out.println("Output from Server .... \n");
			System.out.println(responseDto.getTransactions().get(0));

		} catch (Exception e) {

			e.printStackTrace();

		}

	}
}